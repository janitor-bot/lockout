# lockout needs to check every minute whether it's time to unlock
*/1 * * * *	root	/usr/bin/lockout unlock >/dev/null 2>&1

# rebooting *MUST* restore the root password
@reboot	        root	/usr/bin/lockout unlock force >/dev/null 2>&1
